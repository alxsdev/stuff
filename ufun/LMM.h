#include <iostream>
#include <cmath>
#include <cerrno>
#include <cfenv>
#include <cstring>
#include <vector>
#include <random>
#include <armadillo>

class LMM{

   //constants
   float alpha1;
   float alpha2;

   

   
   //parameters
    float A;
    float B;
    float C;
    float P;
    float K;
    float E;
    arma::vec rateStructure;
    std::random_device rd{};
    std::mt19937 gen{rd()};
    


    public:
      // constant initiate
      void init(std::vector<float> yc,std::vector<float> params){
         alpha1=yc.at(0);
         alpha2=yc.at(1);
         A=params.at(0);
         B=params.at(1);
         C=params.at(2);


         
      }
      //robeto calculate volatility
      double robenato(const double a,const double b,const double c,double T){

         float res= (a+b*T)*exp(-c*T);

         return res;
      }

      
      double Varfun(const float oldv,const float k,const float e,const float theta, const float deltaT,const int Wt)
      {


            double mean=deltaT*(theta-std::fabs(oldv))*k;
            double var=e*(std::sqrt(std::fabs(oldv)*deltaT))*Wt;
            double res = oldv+mean+var;

            return res;

      };


      
      
      std::vector<float> getparams(void){

         return std::vector<float>({A,B,C,P,K,E});

      }

      int impliedVol(std::vector<float> pca, std::vector<float> robo, std::vector<float> varParam, const float rho, const float t){

         double rbt = robenato(robo.at(0),robo.at(1),robo.at(2),t);
         

         return 0;
      }

      //NOTE random number generator
      double randNorm(int n, float mean,float var){
         std::normal_distribution<> d{mean,var};

      
         return d(gen);
      }

      // output: array of robonato volatility given an array of T
      arma::vec blackvolbyrebonato(arma::vec::fixed<3> params,arma::vec tlist){

         tlist.transform( [&](double &val) { return robenato(params.at(0),params.at(1),params.at(2),val); } );

         return tlist;

      }
      //get the benchmark volatility matrix
      arma::mat benchmarkVol(arma::mat market){



      }
      //correlation function of two brownie
      double Corfun(int i,int j,double beta){


      }











};