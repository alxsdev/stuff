# this is where all codes live

```bash

#!/bin/bash
apt-get install -yq python3 git cmake autoconf build-essentials
# openmpi stuff 
apt-get install -yq libopenmpi-dev libscalapack-openmpi-dev libopenblas-base libopenblas-dev libcurl4-openssl-dev

ldconfig

git clone https://bitbucket.hdfgroup.org/scm/hdffv/hdf5.git && cd hdf5 
mkdir build
cd build
cmake ../ -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpic++ -DBUILD_SHARED_LIBS:BOOL=OFF \
-DHDF5_BUILD_CPP_LIB:BOOL=OFF -DHDF5_ENABLE_PARALLEL:BOOL=ON \
-DHDF5_BUILD_HL_LIB:BOOL=ON -DCMAKE_C_FLAGS:STRING=-fPIC \
-DHDF5_ENABLE_TRACE:BOOL=ON \
-DCMAKE_INSTALL_PREFIX=/usr/local

make install
make check



# CC=mpicc CPPFLAGS="-I${H5DIR}/include -I${PNDIR}/include -I/home/szip/szip/include -I/usr/include" \
#   LDFLAGS="-L${H5DIR}/lib -L${PNDIR}/lib -L/home/szip/szip/lib" ./configure \
#   --enable-pnetcdf  --enable-parallel-tests --disable-shared\




# CC=gcc CPPFLAGS="-I/usr/local/include" \
#   LDFLAGS="-L/usr/local/lib" ./configure \
#   --enable-cxx4

# CC=gcc CXXFLAGS="-I${H5DIR}/include -I${PNDIR}/include -I/home/szip/szip/include -I/usr/include" \
#   LDFLAGS="-L${H5DIR}/lib -L${PNDIR}/lib -L/home/szip/szip/lib" ./configure \
#   --enable-pnetcdf  --enable-parallel-tests --disable-shared\






```